"""
This Classes are for Model/View Qt System
Every DataModel Class inherit from DataModel and enables the Table Views for everyone
"""

# from SessionManager


# from DataMOdel
from auth_gui.base_data_models import DataModel, ManageData


class LevelDataModel(DataModel):
    pass


header_level = ['id', 'value', "group", 'description',
                'modify_access', 'read_access', 'multiple_session']

LevelDataModel.set_header(header_level, action='replace')


class LevelManageData(ManageData):
    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent=None, *args, **kwargs)
        self.model_name = 'level'


LevelManageData.add_model('level', LevelDataModel)


class UserDataModel(DataModel):
    pass


header_user = ['id', 'name', 'email', 'level_id']

UserDataModel.set_header(header_user, action='replace')


class UserManageData(ManageData):
    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent=None, *args, **kwargs)
        self.model_name = 'user'


UserManageData.add_model('user', UserDataModel)


class CommandDataModel(DataModel):
    pass


header_command = ['id', 'name', 'description', 'level_id']

CommandDataModel.set_header(header_command, action='replace')


class CommandManageData(ManageData):
    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent=None, *args, **kwargs)
        self.model_name = 'command'


CommandManageData.add_model('command', CommandDataModel)
