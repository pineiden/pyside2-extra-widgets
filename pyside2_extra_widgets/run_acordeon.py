from pyside2_extra_widgets.acordeon import Acordeon
import csv
from PySide2.QtWidgets import QApplication
from quamash import QEventLoop, QThreadExecutor
from PySide2.QtWidgets import QMainWindow
import sys
import asyncio
from PySide2.QtWidgets import QWidget, QScrollArea

AEventLoop = type(asyncio.get_event_loop())

# Una clase intermedia o bridge para juntar
# los eventloop async


class QEventLoopPlus(QEventLoop, AEventLoop):
    pass


def read_data(path='./data/station.csv'):
    data_set = []
    with open(path, 'r') as f:
        read = csv.DictReader(f, delimiter=';')
        for data in read:
            data_set.append(data)
    return data_set


class GuiAcordeon(QMainWindow):
    def __init__(self,
                 rows,
                 parent=None,
                 *args,
                 **kwargs):
        super().__init__(parent)
        self.width = kwargs.get('width')
        self.height = kwargs.get('height')
        self.setGeometry(0, 0, self.width, self.height)
        self.name = kwargs.get('title')
        self.slug = kwargs.get('code')
        self.setWindowTitle(self.name)
        self.create_acordeon(rows)

    def create_acordeon(self, rows):
        acordeon = Acordeon("Datos Estaciones")
        self.setCentralWidget(acordeon)
        data_set = read_data()
        acordeon.load_data(data_set, rows)


def run_interfaz(rows, kwargs):
    app = QApplication(sys.argv)
    loop = QEventLoopPlus(app)
    asyncio.set_event_loop(loop)
    # ex = DragonInterface(loop=loop, **kwargs)
    ex = GuiAcordeon(rows, **kwargs)
    # ex.showMaximized()
    ex.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    rows = ['id', 'name', 'code', 'host', 'port', 'protocol']
    options = {
        'width': 700,
        'height': 350,
        'title': "Ejemplo Acordeon"
    }
    run_interfaz(rows, options)
