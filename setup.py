from setuptools import setup
from networktools.requeriments import read

requeriments = read()

setup(name='pyside2_extra_widgets',
      version='0.1',
      description='QT Extra Widgets for PySide2',
      url='',
      author='David Pineda Osorio',
      author_email='dpineda@uchile.cl',
      license='GPL',
      install_requires=requeriments,
      packages=[],
      zip_safe=False)
