from PySide2.QtCore import QObject, QAbstractTableModel, Qt as qt, QPoint, QModelIndex
from PySide2.QtWidgets import QVBoxLayout, QBoxLayout, QGridLayout, QLayout

from PySide2.QtWidgets import QTableView, QTableWidgetItem, QHeaderView
from PySide2.QtGui import QStandardItem
from networktools.library import my_random_string
from networktools.queue import read_queue
import queue
import delegates as CheckBoxDelegate
from PySide2.QtWidgets import QWidget

"""
When subclassing QAbstractTableModel, you must implement rowCount(), columnCount(), and data(). Default implementations of the index() and parent() functions are provided by QAbstractTableModel. Well behaved models will also implement headerData().

Editable models need to implement setData(), and implement flags() to return a value containing Qt.ItemIsEditable.

Models that provide interfaces to resizable data structures can provide implementations of insertRows()https://www.google.com/search?q=library+gen+ru, removeRows(), insertColumns(), and removeColumns(). When implementing these functions, it is important to call the appropriate functions so that all connected views are aware of any changes:

    An insertRows() implementation must call beginInsertRows() before inserting new rows into the data structure, and it must call endInsertRows() immediately afterwards.
    An insertColumns() implementation must call beginInsertColumns() before inserting new columns into the data structure, and it must call endInsertColhttps://www.youtube.com/watch?v=b3HJXWgPp_kumns() immediately afterwards.
    A removeRows() implementation must call beginRemoveRows() before the rows are removed from the data structure, and it must call endRemoveRows() immediately afterwards.
    A removeColumns() implementation must call beginRemoveColumns() before the columns are removed from the data structure, and it must call endRemoveColumns() immediately afterwards.

Note: Some general guidelines for subclassing models are available in the Model Subclassing Reference.

https://doc.qt.io/archives/4.6/itemviews-addressbook.html
"""

import time


class Container(QWidget):
    def __init__(self, table_name, manager_data, model_name,
                 parent=None, *args, **kwargs):
        """
        table_name :: str, tipo de tabla, nombre como key de diccionario
        manager_data :: data_manager asociado a contendor
        model_name :: nombre del modelo asociado
        parent :: parent widget

        """
        super().__init__(parent)
        self.width = kwargs.get('width')
        self.height = kwargs.get('height')
        self.name = kwargs.get('title')
        self.slug = kwargs.get('code')
        self.data_manager = manager_data
        self.model_name = model_name
        self.create_table(table_name)

    def create_table(self, table_name):
        """
        self :: parent widget
        table_name :: name or title

        """
        idt = self.data_manager.init_table(
            table_name, self)
        model = self.data_manager.get_model(idt)
        self.model = model
        self.main_table = idt
        self.set_layout()
        self.event_table(idt)

    def set_layout(self, layout=None):
        if issubclass(type(layout), QLayout):
            self.setLayout(layout)
        else:
            self.setLayout(QGridLayout())

    def event_table(self, idt):
        """
        We are a widget, so we have to rescue the layout
        and put the widget table on the container

        """
        layout = self.layout()
        table_widget = self.data_manager.get_table(idt)
        table_widget.doubleClicked.connect(self.check_celda)
        layout.addWidget(table_widget)

    def check_celda(self, item):
        model = self.data_manager.get_model(self.main_table)
        model.change_checkbox(item)

    def add_data(self, idd, data):
        idt = self.main_table
        model = self.data_manager.get_model(idt)
        model.add_data_row(idd, data)

    def drop_data(self, idd):
        idt = self.main_table
        model = self.data_manager.get_model(idt)
        print(model.data_set)
        model.delete_data(idd)
