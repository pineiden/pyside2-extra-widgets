from PySide2.QtCore import QObject, QAbstractTableModel, Qt as qt, QPoint, QModelIndex
from PySide2.QtWidgets import QVBoxLayout, QBoxLayout, QGridLayout, QLayout

from PySide2.QtWidgets import QTableView, QTableWidgetItem, QHeaderView
from PySide2.QtGui import QStandardItem
from networktools.library import my_random_string
from networktools.queue import read_queue
import queue
import delegates as CheckBoxDelegate
from PySide2.QtWidgets import QWidget

"""
When subclassing QAbstractTableModel, you must implement rowCount(), columnCount(), and data(). Default implementations of the index() and parent() functions are provided by QAbstractTableModel. Well behaved models will also implement headerData().

Editable models need to implement setData(), and implement flags() to return a value containing Qt.ItemIsEditable.

Models that provide interfaces to resizable data structures can provide implementations of insertRows()https://www.google.com/search?q=library+gen+ru, removeRows(), insertColumns(), and removeColumns(). When implementing these functions, it is important to call the appropriate functions so that all connected views are aware of any changes:

    An insertRows() implementation must call beginInsertRows() before inserting new rows into the data structure, and it must call endInsertRows() immediately afterwards.
    An insertColumns() implementation must call beginInsertColumns() before inserting new columns into the data structure, and it must call endInsertColhttps://www.youtube.com/watch?v=b3HJXWgPp_kumns() immediately afterwards.
    A removeRows() implementation must call beginRemoveRows() before the rows are removed from the data structure, and it must call endRemoveRows() immediately afterwards.
    A removeColumns() implementation must call beginRemoveColumns() before the columns are removed from the data structure, and it must call endRemoveColumns() immediately afterwards.

Note: Some general guidelines for subclassing models are available in the Model Subclassing Reference.

https://doc.qt.io/archives/4.6/itemviews-addressbook.html
"""

import time


class DataModel(QAbstractTableModel):
    header = ['code', 'station_name', 'lat', 'lon']
    static_header = {0, 1, 2, 3}

    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent)
        self.editable_columns = set()
        self.checkable_columns = set()
        self.data_set = list()
        self.data_idd = dict()
        self.queue = kwargs.get('queue', queue.Queue())

    @classmethod
    def set_header(cls, value, action='append',):
        actions = {
            'append': cls.header.append,
            'remove': cls.header.remove,
            'insert': cls.header.insert,
            'replace': cls.replace
        }
        if type(value) is not list or type(value) is not tuple:
            args = [value]
        else:
            args = value
        actions.get(action)(*args)

    @classmethod
    def set_static_header(cls, value):
        cls.static_header = value

    @classmethod
    def replace(cls, header, *args):
        cls.header = header

    def clean_checkable(self):
        self.checkable_columns = set()

    def clean(self):
        self.checkable_columns = set()
        self.editable_columns = set()
        self.data_idd = dict()
        self.data_set = list()

    def rowCount(self, parent=None):
        return len(self.data_set)

    def columnCount(self, parent=None):
        return len(self.header)

    def data(self, index, role=qt.DisplayRole):
        """
        Returns the data stored under the given role for the item referered to by the index

        """
        rows = self.rowCount()
        if not index.isValid() or \
           not (0 <= index.row() and index.row() < rows):
            return None
        row = index.row()
        col = index.column()
        if role == qt.DisplayRole and role != qt.CheckStateRole:
            idd_data = self.data_set[row]
            valid_list = self.data_valid(idd_data)
            if col < len(valid_list):
                data_valid_col = valid_list[col]
                return data_valid_col
            else:
                return ""
        elif role == qt.DisplayRole and col in self.checkable_columns:
            idd_data = self.data_set[row]
            valid_list = self.data_valid(idd_data)
            if col < len(valid_list):
                data_valid_col = valid_list[col]
                return data_valid_col
            else:
                return ""
        elif role == qt.CheckStateRole and col in self.checkable_columns:
            idd_data = self.data_set[row]
            value = self.data_valid(idd_data)[col]
            if value:
                return qt.Checked
            else:
                return qt.Unchecked
        else:
            return None

    def add_data_row(self, idd, data):
        row = self.rowCount()+1
        self.data_set.append(idd)
        self.data_idd.update(
            {idd: {'data': data, 'position': (row-1, 0)}})
        # create new row
        # setdata
        self.insertRow(row)
        data_valid = self.data_valid(idd)
        for j, column in enumerate(self.header):
            value = data_valid[j]
            point_index = self.createIndex(row, j)
            # role = qt.EditRole by default
            self.setData(point_index, value, role=qt.EditRole)
        return data_valid

    def data_valid(self, idd_data):
        data = self.data_idd.get(idd_data).get('data')
        data_valid = []
        for i, key in enumerate(self.header):
            if i in self.checkable_columns:
                column_name = key
                data_q = data.get('checkbox').get(column_name)
                data_valid.append(data_q)
            else:
                data_valid.append(data.get(key))
        return data_valid

    def change_checkbox(self, item):
        row = item.row()
        idd = self.data_set[row]
        elem = self.data_idd.get(idd)
        data = elem.get('data')
        column = item.column()
        print("Data to change->", data)
        if column in self.checkable_columns:
            column_name = self.header[column]
            data_check = data.get('checkbox')
            value = not data_check.get(column_name)
            data_check.update({column_name: value})
            print("Data changed ->", data, data_check)
            self.setData(item, value, qt.CheckStateRole)
            return value, idd, data, column_name
        else:
            return False, idd, data, ""

    def add_data_column(self,
                        window_data,
                        position=None,
                        default_value=False,
                        editable=True,
                        checkable=False,
                        role=qt.DisplayRole):
        """
        Add a column with some default value
        And the value is charged over the field type
        """
        name = window_data
        self.set_header(name)
        # column count counsider the new column
        # so the position must be less in 1
        # if position is over len(header)
        if position:
            if position > self.columnCount() or position < 0:
                position = self.columnCount()-1
        else:
            position = self.columnCount()-1
        if editable:
            self.editable_columns.add(position)
        if checkable:
            self.checkable_columns.add(position)
            role = qt.ItemIsUserCheckable
        print("Chckable columns", self.checkable_columns)
        self.insertColumn(position)
        self.set_column_value(position, name, default_value, role)

    def set_column_value(self, col_position, window_name, value=False, role=qt.DisplayRole):
        print("set_column_value")
        if col_position <= self.columnCount():
            for key, elem in self.data_idd.items():
                data = elem.get('data')
                print("Adding column->", elem)
                data_checker = data.get('checkbox', {})
                print('data checker', data_checker)
                if data_checker:
                    data_checker.update({window_name: value})
                else:
                    print("Create new checkbox column")
                    data.update({'checkbox': {window_name: value}})
                elem.update({'data': data})
                print("Elem post->", elem)
                valid = self.data_valid(key)
                row = elem.get('position')[0]
                point_index = self.createIndex(row, col_position)
                self.setData(point_index, valid, role)

    def drop_data(self, idd):
        row = self.data_idd.get(idd).get('position')[0]
        del self.data_idd[idd]
        self.data_set.remove(idd)
        self.refresh_position(row)
        # clean line in position
        # refresh position value on every data_idd element
        # if position > row, -> row -= 1

    def refresh_position(self, row, op=lambda val: val-1):
        """
        op is the default function
        """
        data = self.data_idd
        for key, value in data.items():
            row_elem = value.get('position')[0]
            if row_elem > row:
                value.update({'position': (op(row_elem), 0)})

    def refresh_column_position(self, column, op=lambda val: val-1):
        """
        op is the default function
        """
        data = self.data_idd
        for key, value in data.items():
            row_elem, col_elem = value.get('position')
            if col_elem > column:
                value.update({'position': (row_elem, op(col_elem))})

    def headerData(self, section, orientation, role=qt.DisplayRole):
        if role == qt.DisplayRole and orientation == qt.Horizontal:
            if section < len(self.header):
                return self.header[section]
            else:
                return None
        else:
            return None

    def setData(self, index, value, role=qt.EditRole):
        # emit DataChanged (at the end)
        """

        """
        if index.isValid() and role == qt.EditRole:
            # index_a corner top-left
            # index_b corner bottom-right
            index_a = index
            index_b = index
            self.dataChanged.emit(index_a, index_b)
            return True
        elif index.isValid() and role == qt.CheckStateRole:
            index_a = index
            index_b = index
            """
            Change data from this index
            """
            row = index.row()
            col = index.column()
            self.dataChanged.emit(index_a, index_b)
            return True
        return False

    def delete_data(self, idd):
        print("Delete data, data-set", self.data_set)
        row = self.data_set.index(idd)
        self.removeRow(row)

    def insertRow(self, row, parent=QModelIndex()):
        self.insertRows(row)

    def insertRows(self, row, rows=1, parent=QModelIndex()):
        """
        bool TableModel::insertRows(int position, int rows, const QModelIndex &index)
        {
            Q_UNUSED(index);
            beginInsertRows(QModelIndex(), position, position+rows-1);

            for (int row=0; row < rows; row++) {
                QPair<QString, QString> pair(" ", " ");
                listOfPairs.insert(position, pair);
            }

            endInsertRows();
            return true;
        }
        """
        self.beginInsertRows(parent, row, row + rows - 1)
        def despl(position): return position+rows-1
        self.refresh_position(row, op=despl)
        self.endInsertRows()
        """
        columns = self.columnCount()
        index_a = QPoint(row, 0)
        index_b = QPoint(row+rows-1, columns)
        self.dataChanged(index_a, index_b)
        """
        return True

    def insertColumn(self, column, parent=QModelIndex()):
        self.insertColumns(column)

    def insertColumns(self, column, columns=1, parent=QModelIndex()):
        """
        bool TableModel::insertRows(int position, int rows, const QModelIndex &index)
        {
            Q_UNUSED(index);
            beginInsertRows(QModelIndex(), position, position+rows-1);

            for (int row=0; row < rows; row++) {
                QPair<QString, QString> pair(" ", " ");
                listOfPairs.insert(position, pair);
            }

            endInsertRows();
            return true;
        }
        """
        self.beginInsertColumns(parent, column, column + columns - 1)
        def despl(position): return position+columns-1
        self.refresh_column_position(column, op=despl)
        self.endInsertColumns()
        """
        columns = self.columnCount()
        index_a = QPoint(row, 0)
        index_b = QPoint(row+rows-1, columns)
        self.dataChanged(index_a, index_b)
        """
        return True

    def removeRow(self, row):
        self.removeRows(row)

    def removeRows(self, row, rows=1, index=QModelIndex()):
        print("Removing at row: %s" % row)
        self.beginRemoveRows(QModelIndex(), row, row + rows - 1)
        print("===DATA SET, remove rows===", self.data_set)
        idd_data = self.data_set[row]
        self.drop_data(idd_data)
        self.endRemoveRows()
        return True

    def flags(self, index):
        if index.column() in self.editable_columns.difference(self.checkable_columns):
            return qt.ItemIsEnabled | qt.ItemIsSelectable | qt.ItemIsEditable
        elif index.column() in self.editable_columns & self.checkable_columns:
            return qt.ItemIsEnabled | qt.ItemIsSelectable | qt.ItemIsUserCheckable
        else:
            return qt.ItemIsEnabled | qt.ItemIsSelectable


class ManageData(QObject):
    """
    ManageData has tables(DataModel) and data assigned to some table
    """
    tables = dict()
    codes = dict()
    all_models = dict()
    parent_widgets = dict()
    all_data = dict()
    idds = set()
    idts = set()
    models = {'default': DataModel, 'data_model': DataModel}

    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent)
        self.default_table = None
        self.channel = None
        self.model_name = 'default'

    @classmethod
    def set_default_model(cls, model_class):
        cls.add_model('default', model_class)

    @classmethod
    def add_model(cls, name, model_class):
        cls.models.update({name: model_class})

    def add_data(self, data, idt=None):
        """
        idt :: id de tabla
        data :: dato a asociar a tabla
        """
        code = data.get('code')
        print(idt)
        model_instance = self.get_model(idt)
        idd = ManageData.set_idx(self.idds)
        self.all_data.update({idd: data})
        if model_instance and code:
            model_instance.add_data_row(idd, data)
            self.codes.update({code: {'idd': idd, 'table': idt}})
        else:
            self.codes.update({code: {'idd': idd, 'table': None}})

    def assign_data2table(self, code, table_id):
        """
        Set some data identified by some code, that store the id on the 'codes' dict, and relate this
        with some active table on the system

        """
        self.codes.update[code].update({'table': table_id})
        idd = self.codes[code].get('idd')
        data = self.all_data.get(idd)
        model_instance = self.get_model(table_id)
        if model_instance and data:
            model_instance.add_data(data)

    def unset_data_from_table(self, code):
        """
        Drop data from table
        """
        table_id = self.codes[code].get('table')
        self.codes[code].update({'table': None})
        idd = self.codes[code].get('idd')
        model_instance = self.get_model(table_id)
        model_instance.drop_data(idd)

    def drop_data(self, code):
        # unset from
        # delete from all_data
        del self.codes[code]
        idd = self.code.get(code).get('idd')
        self.unset_data_from_table(code)
        del self.all_data[idd]
        self.idds.remove(idd)

    def update_data(self, code, field, value):
        idd = self.codes[code].get('idd')
        data = self.all_data.get(idd)
        data.update({field: value})

    def init_new_model(self, idt, parent_widget, model_name='default'):
        if model_name not in self.models:
            model_name = 'default'
        model = self.models.get(model_name)(parent=parent_widget)
        self.all_models.update({idt: {'model': model, 'name': None}})
        return model

    def get_table(self, idt):
        return self.tables.get(idt)

    def get_model(self, idt):
        if idt in self.all_models:
            return self.all_models.get(idt).get('model')
        else:
            return None

    def init_table(self, name, parent_window, clean=True):
        """
        This method create a Table View and asociate with a new model
        related with the model_name

        """
        model_name = self.model_name
        idt = ManageData.set_idx(self.idts)
        table_widget = QTableView(parent=parent_window)
        horizontal_header = table_widget.horizontalHeader()
        horizontal_header.setSectionResizeMode(QHeaderView.Stretch)
        self.tables.update({idt: table_widget})
        model = self.init_new_model(idt, table_widget, model_name)
        if clean:
            model.clean()
        table_widget.setModel(model)
        self.parent_widgets.update({idt: table_widget})
        print("Init table ->", idt)
        return idt

    def table_anchor(self, table_id, anchor_widget):
        """
        Asociate a superior widget or window with the table

        """
        table_widget = self.tables.get(table_id)
        if table_widget:
            anchor_widget.addWidget(table_widget)

    def drop_table(self, idt):
        """
        Dop the table from interface
        Must close or delete from superior widget
        """
        [self.unset_data_from_table(code)
         for code, value in self.codes.items()
         if value.get('table') == idt]
        parent_w = self.parent_widgets.get(idt)
        table_w = self.all_models.get(idt)
        parent_w.removeWidget(table_w)
        del self.all_models[idt]
        del self.tables[idt]

    def load_settings(self, filename, path='./settings/%s'):
        """
        Load the settings from a file
        Open all windows with their data

        """
        file_path = path.format(filename)

    def save_settings(self, filename, path='./settings/%s'):
        """
        Save the settings on a file
        Save:
        - windows
        - tables
        - data on windows
        """
        file_path = path.format(filename)
        # number of tables
        # name of the tables
        # size of the windows
        # data assigned to every table

    @staticmethod
    def set_idx(id_set, uin=4):
        """
        Defines a new id for task related to collect data insice a worker, check if exists
        """
        isg = my_random_string(uin)
        while True:
            if isg not in id_set:
                id_set.add(isg)
                break
            else:
                isg = my_random_string(uin)
        return isg

    def set_channel(self, channel):
        self.channel = channel
