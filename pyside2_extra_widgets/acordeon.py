from PySide2.QtWidgets import QWidget, QScrollArea, QListView
from PySide2.QtWidgets import QLabel, QLineEdit, QScrollBar, QTextBrowser
from PySide2.QtWidgets import (QVBoxLayout,
                               QGridLayout,
                               QHBoxLayout)
from PySide2.QtWidgets import QPushButton
from PySide2.QtCore import (QVariantAnimation,
                            QAnimationGroup,
                            QAbstractAnimation,
                            QPropertyAnimation,
                            QEasingCurve, QRect)
from PySide2.QtCore import Qt

import time
layouts = {
    'vertical': QVBoxLayout,
    'horizontal': QHBoxLayout,
    'grid': QGridLayout
}

def clear_layout(layout):
    print("Cantidad de widgets->", layout.count())
    for i in reversed(range(layout.count())): 
        layout.itemAt(i).widget().deleteLater()

class Pair(QWidget):
    """
    Put a title and content in horizontal view

    """
    def __init__(self, title, data,
                 layout='horizontal',
                 parent=None,
                 *args, **kwargs):
        super().__init__(parent)
        self.title = title
        self.data = data
        self.layout = layouts.get(layout, QVBoxLayout)(self)
        self.setLayout(self.layout)
        self.create_content()

    def create_content(self):
        label = QLabel(self.title)
        label.setMargin(5)
        label.setFixedWidth(100)
        self.layout.addWidget(label)
        if len(self.data)<=100:
            data = QLineEdit()
            data.setReadOnly(True)
            data.setText(self.data)
            self.layout.addWidget(data)
        else:
            data = QTextBrowser()
            data.setReadOnly(True)
            data.setText(self.data)
            self.layout.addWidget(data)
        
class AcordeonItem(QWidget):
    """
    Put information from dicts into a  QTextBrowserselectable
    widget that shows itself when select the item

    """
    def __init__(self, title, data, rows,
                 layout='vertical',
                 size=(10, 600),
                 parent=None,
                 time_animation=1000,
                 size_0=(0, 50, 600, 0),
                 size_1=(0, 50, 600, 300),
                 *args, **kwargs):
        super().__init__(parent)
        self.height, self.width = size
        self.title = title
        self.data = data
        self.layout = layouts.get(layout, QVBoxLayout)(self)
        self.time_animation = time_animation
        self.setLayout(self.layout)
        self.rect_init = QRect(*size_0)
        self.rect_end = QRect(*size_1)
        self.set_rows(rows)

    def create_content(self):
        title_button = QPushButton(self.title)
        self.layout.addWidget(title_button)
        title_button.clicked.connect(self.toggle_content)
        # add widget with the contents
        self.content_widget = QWidget()
        self.content_widget.setVisible(False)
        self.layout.addWidget(self.content_widget)
        self.content_layout = layouts.get('vertical')(self.content_widget)
        self.load_data()
        self.status = True

    def update_info(self, new_info_dict):
        self.data.update(new_info_dict)
        clear_layout(self.content_layout)
        self.load_data()

    def set_rows(self, rows=[]):
        self.rows = rows

    def toggle_content(self):
        if self.status:
            time.sleep(.3)
            self.content_widget.setVisible(True)
            self.status = False
        else:
            #self.hide_content()
            time.sleep(.3)
            self.content_widget.setVisible(False)
            self.status = True

    def load_data(self):
        content = [(key, value) for key, value in self.data.items()
                   if key in self.rows]
        for key, value in content:
            info = Pair(key, value, parent=self)
            self.content_layout.addWidget(info)

    def show_content(self):
        self.animation = QPropertyAnimation(self.content_widget, b"geometry")
        self.animation.setDuration(self.time_animation)
        self.animation.setEasingCurve(QEasingCurve.InQuad);
        self.animation.setStartValue(self.rect_init)
        self.animation.setEndValue(self.rect_end)
        self.animation.start()

    def hide_content(self):
        self.animation = QPropertyAnimation(self.content_widget, b"geometry")
        self.animation.setDuration(self.time_animation)
        self.animation.setEasingCurve(QEasingCurve.InQuad);
        self.animation.setStartValue(self.rect_end)
        self.animation.setEndValue(self.rect_init)
        self.animation.start()

class Acordeon(QWidget):
    """
    Show a collection of desplegable items

    """
    def __init__(self, title,
                 size=(600, 500),
                 parent=None,
                 *args, **kwargs):
        super().__init__(parent)
        self.setFixedHeight(800)

        self.height, self.width = size
        self.title = title
        self.content_widget = QWidget()
        self.layout = layouts.get('vertical',
                                  QVBoxLayout)()
        self.content_widget.setLayout(self.layout)
        # ScrollArea def
        self.scrollarea = QScrollArea()
        self.scrollarea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scrollarea.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scrollarea.setWidgetResizable(True)
        self.scrollarea.setWidget(self.content_widget)

        # Add widgets to layout
        #self.layout.addWidget(self.content_widget)
        #self.layout.setGeometry(QRect(0, 0, *size))
        self.items = {}
        self.v_layout = layouts.get('vertical', QVBoxLayout)(self)
        self.v_layout.addWidget(self.scrollarea)
        self.setLayout(self.v_layout)

    def load_data(self, data_list, rows, clean=False):
        if clean:
            clear_layout(self.layout)
        for data in data_list:
            name = data.get('id')+" "+data.get('name')
            del data['name']
            acordeon = AcordeonItem(name, data, rows, parent=self)
            self.items.update({name:acordeon})
            self.layout.addWidget(acordeon)
            acordeon.create_content()
